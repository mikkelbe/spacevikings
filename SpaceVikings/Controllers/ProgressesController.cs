﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SpaceVikings.DAL;
using SpaceVikings.Models;

namespace SpaceVikings.Controllers
{
    public class ProgressesController : ApiController
    {
        private SpaceVikingsContext db = new SpaceVikingsContext();

        // GET: api/Progresses
        public IQueryable<Progress> GetProgresses()
        {
            return db.Progresses;
        }

        // GET: api/Progresses/5
        [ResponseType(typeof(Progress))]
        public IHttpActionResult GetProgress(int id)
        {
            Progress progress = db.Progresses.Find(id);
            if (progress == null)
            {
                return NotFound();
            }

            return Ok(progress);
        }

        // PUT: api/Progresses/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProgress(int id, Progress progress)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != progress.ProgressId)
            {
                return BadRequest();
            }

            db.Entry(progress).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgressExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Progresses
        [ResponseType(typeof(Progress))]
        public IHttpActionResult PostProgress(Progress progress)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Progresses.Add(progress);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = progress.ProgressId }, progress);
        }

        // DELETE: api/Progresses/5
        [ResponseType(typeof(Progress))]
        public IHttpActionResult DeleteProgress(int id)
        {
            Progress progress = db.Progresses.Find(id);
            if (progress == null)
            {
                return NotFound();
            }

            db.Progresses.Remove(progress);
            db.SaveChanges();

            return Ok(progress);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgressExists(int id)
        {
            return db.Progresses.Count(e => e.ProgressId == id) > 0;
        }
    }
}