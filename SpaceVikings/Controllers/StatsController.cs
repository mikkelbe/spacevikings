﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using SpaceVikings.DAL;
using SpaceVikings.Models;

namespace SpaceVikings.Controllers
{
    public class StatsController : ApiController
    {
        private SpaceVikingsContext db = new SpaceVikingsContext();

        // GET: api/Stats
        public IQueryable<Stats> GetStats()
        {
            return db.Stats;
        }

        // GET: api/Stats/5
        [ResponseType(typeof(Stats))]
        public IHttpActionResult GetStats(int id)
        {
            Stats stats = db.Stats.Find(id);
            if (stats == null)
            {
                return NotFound();
            }

            return Ok(stats);
        }

        // PUT: api/Stats/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutStats(int id, Stats stats)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != stats.StatsId)
            {
                return BadRequest();
            }

            db.Entry(stats).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StatsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Stats
        [ResponseType(typeof(Stats))]
        public IHttpActionResult PostStats(Stats stats)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Stats.Add(stats);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = stats.StatsId }, stats);
        }

        // DELETE: api/Stats/5
        [ResponseType(typeof(Stats))]
        public IHttpActionResult DeleteStats(int id)
        {
            Stats stats = db.Stats.Find(id);
            if (stats == null)
            {
                return NotFound();
            }

            db.Stats.Remove(stats);
            db.SaveChanges();

            return Ok(stats);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StatsExists(int id)
        {
            return db.Stats.Count(e => e.StatsId == id) > 0;
        }
    }
}