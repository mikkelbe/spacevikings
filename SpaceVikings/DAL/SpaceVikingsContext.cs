﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using SpaceVikings.Models;

namespace SpaceVikings.DAL
{
    public class SpaceVikingsContext : DbContext
    {
        public SpaceVikingsContext() : base("SpaceVikingsContext")
        {

        }

        public DbSet<Progress> Progresses { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Stats> Stats { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}