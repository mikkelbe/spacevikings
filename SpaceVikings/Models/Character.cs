﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SpaceVikings.Models
{
    public class Character
    {
        public int CharacterId { get; set; }
        public string Name { get; set; }
        public string EquippedWeapon { get; set; }
        public string EquippedShield { get; set; }
        public string EquippedHelmet { get; set; }
        public string EquippedBody { get; set; }
        public string EquippedLegs { get; set; }
        public string EquippedFeet { get; set; }
        public virtual Inventory Inventory { get; set; }
        public int InventoryId { get; set; }
        public virtual Stats Stats { get; set; }
        public int StatsId { get; set; }
    }
}