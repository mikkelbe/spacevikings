﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SpaceVikings.Models
{
    public class Game
    {
        public int GameId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Character> Characters { get; set; }
        public virtual Progress Progress { get; set; }
        public int ProgressId { get; set; }
    }
}