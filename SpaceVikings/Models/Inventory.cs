﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpaceVikings.Models
{
    public class Inventory
    {
        public int InventoryId { get; set; }
        public int Size { get; set; }
        public virtual ICollection<Item> Items { get; set; }
    }
}