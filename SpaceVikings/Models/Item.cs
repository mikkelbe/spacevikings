﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SpaceVikings.Models
{
    public class Item
    {
        public int ItemId { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public virtual Stats Stats { get; set; }
        public int StatsId { get; set; }
        [JsonIgnore]
        public virtual ICollection<Inventory> Inventories { get; set; }
    }
}