﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpaceVikings.Models
{
    public class Progress
    {
        public int ProgressId { get; set; }
        public int Level { get; set; }
        public int Checkpoint { get; set; }
        public int TimePlayed { get; set; }
    }
}