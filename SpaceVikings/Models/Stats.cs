﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpaceVikings.Models
{
    public class Stats
    {
        public int StatsId { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Stamina { get; set; }
        public int Agility { get; set; }
    }
}